package scene;

import javafx.scene.input.KeyCode;

import java.util.ArrayList;

public class GameSceneManager {
    private static GameSceneManager instance;

    public static final int MENU = 0;
    public static final int GamePlay = 1;

    private int currentLevel;
    private ArrayList<GameScene> scenes;

    public static GameSceneManager getInstance(){
        if (instance == null){
            instance = new GameSceneManager();
        }
        return instance;
    }

    private GameSceneManager(){
        scenes = new ArrayList<>();
        currentLevel = MENU;
        scenes.add(new MenuScene());
        scenes.add(new GamePlay());
    }

    public void changeSceneLevel(int sceneLevel){
        currentLevel = sceneLevel;
        scenes.get(sceneLevel).init();
    }

    public GameScene getCurrentGameScene(){
        return scenes.get(currentLevel);
    }

    public  void keyPressed(KeyCode keyCode){
        getCurrentGameScene().keyPressed(keyCode);
    }

    public void keyReleased(KeyCode keyCode){
        getCurrentGameScene().keyReleased(keyCode);
    }
}





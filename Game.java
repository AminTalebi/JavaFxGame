import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TimelineBuilder;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import multimedia.MultimediaHandler;
import scene.GamePlay;
import scene.GameSceneManager;

public class Game extends Application implements EventHandler<KeyEvent> {

    public static final String GAMETITLE = "A SAMPLE JAVAFX GAME";
    public static final int WIDTH = 640;
    public static final int HEIGHT = 480;
    public static final int FPS = 60;
    private GameWindow gameWindow;
    private Timeline gameTimeLine;
    private GameSceneManager gameSceneManager;
    private MediaPlayer mediaPlayer;

    private boolean playing;

    @Override
    public void init() throws Exception{
        super.init();

        gameWindow = new GameWindow(WIDTH, HEIGHT);
        gameSceneManager = GameSceneManager.getInstance();

        mediaPlayer = new MediaPlayer(MultimediaHandler.getMusicByName("bg.mp3"));
        mediaPlayer.setOnEndOfMedia(() -> mediaPlayer.seek(Duration.ZERO));

        Duration secondsPerFrame = Duration.millis(1000 / (float) FPS);
        KeyFrame keyFrame = new KeyFrame(secondsPerFrame, event -> {
           if (GameSceneManager.getInstance().getCurrentGameScene() instanceof GamePlay) {
               if(!playing){
                   mediaPlayer.play();
                   playing = true;
               }
           }
           else {
               mediaPlayer.stop();
               playing = false;
           }

           gameWindow.update();
           gameWindow.render();
        });

        gameTimeLine = TimelineBuilder.create().cycleCount(Animation.INDEFINITE).keyFrames(keyFrame).build();
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane root = new Pane();
        root.getChildren().add(gameWindow);
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        primaryStage.setTitle(GAMETITLE);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);

        scene.setOnKeyPressed(this);
        scene.setOnKeyReleased(this);

        gameTimeLine.play();
        primaryStage.show();
    }

    @Override
    public synchronized void stop() throws Exception{
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(KeyEvent event) {
        if(event.getEventType() == KeyEvent.KEY_PRESSED) {
            gameSceneManager.keyPressed(event.getCode());
        }
        if(event.getEventType() == KeyEvent.KEY_RELEASED) {
            gameSceneManager.keyReleased(event.getCode());
        }
    }
}
